from random import randint
import movelib
import copy
import numpy as np

class Joueur():

	def __init__(self, jeu, color):
		self.jeu = jeu
		self.color = color

	def donne_coup(self, jeu):
		pass
	

class Humain(Joueur):
	pass
	
	
class IA(Joueur):
	pass

class IARandom(IA):
	def donne_coup(self, jeu):
		arrayLength = len(jeu.goban.liste_coups_oks()) - 1
		randomNumber = randint(0, arrayLength)
		return jeu.goban.liste_coups_oks()[randomNumber]

class IAMonteCarlo(IA):
   
	def simulation(self, jeu):
		while not jeu.partie_finie:
			arrayLength = len(jeu.goban.liste_coups_oks()) - 1
			randomNumber = randint(0, arrayLength)
			jeu.jouer(jeu.goban.liste_coups_oks()[randomNumber])
		return jeu.score()*self.color
        
    
	def donne_coup(self, jeu):
		coupMax= []
		ptMax = -100
		coupTab=[]
		cpOk=jeu.goban.liste_coups_oks()
		coupTab=[0 for i in range(len(cpOk))]
		max_nb_evals = 5
		nbcoupsjouer=0
        
		for i in range(max_nb_evals):  #on va faire n simulations avec le meme point de départ
			for j in range(0, len(cpOk)-1):#on teste tous les possibilitées
				jeuCp = jeu.copy()#on créer une copie du jeu
				jeuCp.jouer(cpOk[j])#on joue le n-ième coup ok
				coupTab[j]=coupTab[j]+self.simulation(jeuCp) #on additionne les points obtenu en partant de ce n-ième point    
				nbcoupsjouer=nbcoupsjouer+1
                
		for i in range(0, len(coupTab)):
			if ptMax<coupTab[i]:
				ptMax=coupTab[i]
				coupMax=cpOk[i]
# 		print(nbcoupsjouer)
		return coupMax

class IAMonteCarloBIS(IA):
    
	def __init__(self, jeu, color, max_nb_evals):
		self.jeu = jeu
		self.color = color
		self.max_nb_evals = max_nb_evals
        
	def simulation(self, jeu):
		while not jeu.partie_finie:
			arrayLength = len(jeu.goban.liste_coups_oks()) - 1
			randomNumber = randint(0, arrayLength)
			jeu.jouer(jeu.goban.liste_coups_oks()[randomNumber])
		return jeu.score()*self.color
        
    
	def donne_coup(self, jeu):
		coupMax= []
		ptMax = -100
		coupTab=[]
		cpOk=jeu.goban.liste_coups_oks()
		coupTab=[0 for i in range(len(cpOk))]
		nbcoupsjouer=0
        
		for i in range(self.max_nb_evals):  #on va faire n simulations avec le meme point de départ
			for j in range(0, len(cpOk)-1):#on teste tous les possibilitées
				jeuCp = jeu.copy()#on créer une copie du jeu
				jeuCp.jouer(cpOk[j])#on joue le n-ième coup ok
				coupTab[j]=coupTab[j]+self.simulation(jeuCp) #on additionne les points obtenu en partant de ce n-ième point    
				nbcoupsjouer=nbcoupsjouer+1
                
		for i in range(0, len(coupTab)):
			if ptMax<coupTab[i]:
				ptMax=coupTab[i]
				coupMax=cpOk[i]
		return coupMax
        
        
        
        